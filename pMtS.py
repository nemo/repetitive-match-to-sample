"""
Precision match-to-sample test
Based on Alekseichuk et al., 2016 (https://doi.org/10.1016/j.cub.2016.04.035)
"""
from psychopy import visual, data, logging, gui, core, clock
import os
from psychopy.constants import (NOT_STARTED, STARTED, PLAYING, PAUSED,
                                STOPPED, FINISHED, PRESSED, RELEASED, FOREVER)
from psychopy.event import Mouse, xydist                                
import numpy as np 
from itertools import permutations, product
from collections import OrderedDict
from pyniexp.scannersynch import scanner_synch
from utils import dprime

######## CONFIG ########
# Settings
expName = 'precision Match-to-Sample'
EMUL = True # Is it outside the scanner
gridSize = 800 # width and height of the grid
gridXY = 5 # number of cells per axis -> number of cells = gridXY**2
sampleNum = 5 # number of circles (cannot be more than half of the number of cells)
sampleSize = 0.9 # circle size relative to cell size
sampleDuration = 0.4 # duration of the sample

nSample = 20 # number of samples

jitterRange = [1, 2] # jitter between events
responseDuration = 3 # duration of response

# Grid
sampleSize = gridSize / gridXY * sampleSize
cellCoordinates = gridSize/gridXY*(gridXY-1)/2
cellCoordinates = np.asarray(list(product(np.linspace(-cellCoordinates,cellCoordinates,gridXY),repeat=2)))
gridCoordinates = np.asarray([(l,0) for l in  np.linspace(-gridSize/2,gridSize/2,gridXY+1)] + [(0,l) for l in  np.linspace(-gridSize/2,gridSize/2,gridXY+1)])
gridAngles = [90]*(gridXY+1) + [0]*(gridXY+1)

if __name__ == '__main__':
    ######## LOGGING ########
    _thisDir = os.path.dirname(os.path.abspath(__file__))
    os.chdir(_thisDir)

    psychopyVersion = '3.0.0'
    expInfo = {'participant': '', 'session': '1'}
    dlg = gui.DlgFromDict(dictionary=expInfo, title=expName)
    if dlg.OK == False:
        core.quit()  # user pressed cancel
    expInfo['date'] = data.getDateStr()  # add a simple timestamp
    expInfo['expName'] = expName
    expInfo['psychopyVersion'] = psychopyVersion
    expInfo['gridXY'] = gridXY
    expInfo['sampleNum'] = sampleNum
    expInfo['responseDuration'] = responseDuration

    filename = _thisDir + os.sep + u'data/%s_%s_%s' % (expInfo['participant'], expName, expInfo['date'])

    thisExp = data.ExperimentHandler(name=expName, version='',
        extraInfo=expInfo, runtimeInfo=None,
        originPath='D:\\Private\\University of Surrey\\Violante, Ines Dr (Psychology) - NeuroModulationLab\\BayesianOptimisation\\Tasks\\pMTS.py',
        savePickle=True, saveWideText=True,
        dataFileName=filename)
    logFile = logging.LogFile(filename+'.log', level=logging.EXP)
    logging.console.setLevel(logging.WARNING)  # this outputs to the screen, not a file

    ######## PREPARE ########
    sampleTrials = []

    for s in range(0,nSample):
        sampleSelection = np.random.choice(range(0,gridXY**2-1),size=sampleNum*2,replace=False)
        jitters = np.random.random(2)*(jitterRange[1]-jitterRange[0])+jitterRange[0]
        sampleTrials += [OrderedDict([
            ('onsetSample',jitters[0]), 
            ('sample',sampleSelection[0:sampleNum]),
            ('onsetRecall',jitters[1])
            ])]
    loopSample = data.TrialHandler(nReps=1, method='sequential', trialList=sampleTrials, autoLog=False)
    thisExp.addLoop(loopSample)

    # Scanner and buttons
    SSO = scanner_synch(config='config.json',emul_synch=EMUL,emul_buttons=EMUL)
    SSO.set_synch_readout_time(0.5)
    SSO.TR = 2

    SSO.set_buttonbox_readout_time(0.5)
    SSO.buttonbox_timeout = 2 # wait for response for 2 sec
    # buttons: no - yes
    if not(SSO.emul_buttons): SSO.add_buttonbox('Nata')
    else: SSO.buttons = ['1','2'] 
    SSO.start_process()

    # Visual
    win = visual.Window([1280,1024],winType='pyglet',screen=1,monitor='External Monitor HD',units='pix',fullscr = True, autoLog=False, gammaErrorPolicy='ignore')
    mouse = Mouse()
    win.mouseVisible = False
    gridForm = visual.ElementArrayStim(win=win, name='gridForm', nElements=(gridXY+1)*2, sizes=[gridSize,2], xys = gridCoordinates, oris=gridAngles, units='pix', 
            elementTex=np.ones([16,16]), elementMask=np.ones([16,16]), colors=[-1,-1,-1], colorSpace='rgb', autoLog=False)
    responseStim = visual.TextStim(win=win, name='Response',
        text="?", height=gridSize*0.9, wrapWidth=win.size[0], autoLog=False)

    # Timers
    expClock = core.Clock()
    trialClock = core.Clock()
    logging.setDefaultClock(expClock)

    ######## WAIT FOR SYNCH ########
    msg = visual.TextStim(win, text="Press a button to start...", height=gridSize*0.1, wrapWidth=win.size[1], autoLog=False)
    msg.draw()
    win.flip()
    SSO.wait_for_button()

    ######## WAIT FOR SYNCH ########
    msg = visual.TextStim(win, text="Wait for scanner...", height=gridSize*0.1, wrapWidth=win.size[1], autoLog=False)
    msg.draw()
    win.flip()
    SSO.wait_for_synch()
    SSO.reset_clock()
    expClock.reset()

    frameN = -1
    prevSS = SSO.synch_count
    logging.log(level=logging.DATA, msg='Pulse - {:.3f} - {}'.format(SSO.time_of_last_pulse,SSO.synch_count))

    ######## RUN ########
    for thisSample in loopSample:
        # Sample
        trialClock.reset() 
        jitter = thisSample['onsetSample']

        sampleCoordinates = cellCoordinates[thisSample['sample'],:]
        sampleForm = visual.ElementArrayStim(win, nElements=sampleCoordinates.shape[0], sizes=sampleSize, xys = sampleCoordinates, units='pix', 
            elementTex=None, elementMask="circle", colors=[-1,-1,-1], colorSpace='rgb', autoLog=False)
        sampleForm.status = NOT_STARTED   
        
        while trialClock.getTime() < (jitter + sampleDuration):
            # get current time
            t = trialClock.getTime()
            frameN = frameN + 1  # number of completed frames (so 0 is the first frame)

            # update/draw components on each frame
            gridForm.draw()
            
            # *sampleForm* updates
            if t >= jitter and sampleForm.status == NOT_STARTED:
                # keep track of start time/frame for later
                sampleForm.tStart = t
                sampleForm.frameNStart = frameN  # exact frame index
                sampleForm.status = STARTED
                win.logOnFlip(level=logging.EXP, msg='Sample - STARTED - ' + np.array2string(thisSample['sample']))
            
            if sampleForm.status == STARTED:
                sampleForm.draw()

            win.flip()
        
        if sampleForm.status == STARTED:
            sampleForm.status = STOPPED
            win.logOnFlip(level=logging.EXP, msg='Sample - STOPPED')
        
        # Recall
        trialClock.reset() 
        jitter = thisSample['onsetRecall']

        recallForm = visual.ElementArrayStim(win, nElements=gridXY**2, sizes=sampleSize, xys = cellCoordinates, units='pix', 
            elementTex=None, elementMask="circle", colors=[[0,0,0]]*(gridXY**2), colorSpace='rgb', autoLog=False)
        respCollection = []

        responseStim.status = NOT_STARTED
        
        while trialClock.getTime() < (jitter + responseDuration):
            # get current time
            t = trialClock.getTime()
            frameN = frameN + 1  # number of completed frames (so 0 is the first frame)

            # update/draw components on each frame
            gridForm.draw()
            recallForm.draw()
            
            # *sampleForm* updates
            if t >= jitter and responseStim.status == NOT_STARTED:
                # keep track of start time/frame for later
                responseStim.tStart = t
                responseStim.frameNStart = frameN  # exact frame index
                responseStim.setAutoDraw(True)
                win.logOnFlip(level=logging.EXP, msg='Cue - STARTED')
            if t >= jitter+sampleDuration and responseStim.status == STARTED:
                responseStim.status = STOPPED
                responseStim.setAutoDraw(False)
                win.logOnFlip(level=logging.EXP, msg='Cue - STOPPED')
                win.mouseVisible = True
                mouse.setPos([0,0])
            
            if win.mouseVisible and any(mouse.getPressed()):
                tExpResp = expClock.getTime()
                tTrialResp = trialClock.getTime()
                cellDist = [xydist(coord,mouse.getPos()) for coord in cellCoordinates]
                cellIndex = cellDist.index(min(cellDist))
                if not(any([cellIndex == c[1] for c in respCollection])):
                    if len(respCollection) == 0: logging.log(level=logging.EXP, t=tExpResp, msg='Response - STARTED')
                    recColors = recallForm.colors
                    recColors[cellIndex] = [-1,-1,-1]
                    recallForm.colors = recColors
                    respCollection.append([tTrialResp,cellIndex])
                    logging.log(level=logging.EXP, msg='Mouse - {:.3f} - {}'.format(tExpResp,cellIndex))

            win.flip()
        
        win.mouseVisible = False
        if len(respCollection):
            logging.log(level=logging.EXP, t=tExpResp, msg='Response - STOPPED')

            thisExp.addData('resp.onset',respCollection[0][0])
            thisExp.addData('resp.duration',respCollection[-1][0]-respCollection[0][0])
            thisExp.addData('resp.rt',respCollection[0][0]-responseStim.tStart)
            resp = [e[1] for e in respCollection]
            thisExp.addData('resp',resp)
            thisExp.addData('resp.precision',len(set(thisSample['sample']) & set(resp)) * 2.0 / (len(resp)+sampleNum)) # Dice coeff
            thisExp.addData('resp.dprime', dprime(
                len(set(thisSample['sample']) & set(resp)),
                len(set(thisSample['sample']) - set(resp)), 
                len(set(resp) - set(thisSample['sample'])), 
                len(set(range(0,gridXY**2-1)) - set(thisSample['sample']) - set(resp))
                )) # d-prime (hit, miss, fa, crs)
        else: 
            thisExp.addData('resp.precision','miss')
            thisExp.addData('resp.dprime',dprime(0,sampleNum,0,gridXY**2-sampleNum))

        thisExp.nextEntry()
    
    win.flip()
    SSO = None

    ######## EVENTS (BIDS) ########
    logging.flush()

    import csv

    eventFile = filename + '_events.tsv'
    fOut = open(eventFile,'w',newline='')
    ev = csv.writer(fOut, delimiter='\t')
    ev.writerow(['onset','duration','trial_type','precision'])

    fIn = open(filename+'.log')
    log = csv.reader(fIn, delimiter='\t')
    sample = []
    itemToWrite = []
    response = []
    for item in log:
        if len(item) < 2 or item[1].find('EXP') == -1: continue
        if any(item[2].find(evs) >= 0 for evs in ['Sample', 'Cue', 'Response']):
            if len(itemToWrite) == 0:
                itemToWrite = [round(float(item[0]),4), 0, item[2].split(' - ')[0]]
                if item[2].find('Sample') >= 0: sample = np.fromstring(item[2].split(' - ')[2][1:-1],sep=' ')
            else:
                itemToWrite[1] = round(float(item[0]) - itemToWrite[0],4)
                if item[2].find('Response') >= 0 and len(response): 
                    itemToWrite += [len(set(sample) & set(response)) * 2.0 / (len(sample)+len(response))]
                    sample = []
                    response = []
                ev.writerow(itemToWrite)
                itemToWrite = []
        elif item[2].find('Mouse') >= 0:
            response += [int(item[2].split(' - ')[2])]            

    fIn.close()
    fOut.close()