# based on https://lindeloev.net/calculating-d-in-python-and-php/
from scipy.stats import norm
from numpy import array, append, isin, ones, zeros, ceil, dot, sin, cos, pi
from numpy.random import uniform, choice, permutation, randint
from psychopy.visual import Polygon, ShapeStim

Z = norm.ppf

indComplexity = 100

def generate_jitter(rJitter,nJitter,tolJitter):
    jitter = uniform(rJitter[0],rJitter[1],nJitter)
    while not(jitter.mean() >= array(rJitter).mean()-tolJitter and jitter.mean() <= array(rJitter).mean()+tolJitter):
        jitter = uniform(rJitter[0],rJitter[1],nJitter)
    return jitter

def generate_triangle_mask(n):
    mask = ones((n,n))*-1
    mask[0:2,:] = 1
    sel = ceil(array(range(1,n+1))/2).astype(int)-1
    for i in range(1,n): mask[i,sel[i]:-sel[i]] = 1  
    return mask

masks ={
    0: 'circle',
    1: generate_triangle_mask(128),
    2: ones((32,32))
}

def get_neighbor(gridXY,loc):
    m = array(range(gridXY**2)).reshape([gridXY,gridXY])
    pos = [int(loc/gridXY),int(loc%gridXY)]
    return [m[pos[0]+x, pos[1]+y] for x in range(-1,2) for y in range(-1,2) if (pos[0]+x in range(m.shape[0])) and (pos[1]+y in range(m.shape[1])) and (x*y == 0)]

def generate_sample(gridXY=4,sampleNum=4,neighborNumber=1,complexity=1):
    orig = array([randint(0,complexity)*indComplexity+i for i in range(gridXY**2)])
    avail = list(range(gridXY**2))
    doNeighbor = choice(range(1,sampleNum),neighborNumber)
    
    # sample
    sample = array([],dtype=int)
    while (len(sample) < sampleNum) and (len(avail)):
        if any(isin(doNeighbor,len(sample))):
            avail1 = get_neighbor(gridXY,sample[-1]); avail1.remove(sample[-1])
            item = choice(avail1,1)
        else:
            item = choice(avail,1)
        sample = append(sample, item[0])
        [avail.remove(i) for i in get_neighbor(gridXY,item) if any(isin(avail,i))]
    
    # 'anti'-sample
    avail = list(set([i for sublist in [get_neighbor(gridXY,i) for i in sample] for i in sublist]))
    [avail.remove(i) for i in sample]
    return orig[sample], orig[permutation(append(sample,choice(avail,size=len(sample),replace=False)))]

def rotateCoord(xy,deg):
    return [dot(xy,[cos(deg*pi/180),sin(deg*pi/180)]),dot(xy,[-sin(deg*pi/180),cos(deg*pi/180)]),]

class ArrayStim:

    def __init__(self,win, nElements=1, sizes=1, xys=[[0,0]], oris=[], units='', 
                elementFill=False, elementMask="circle", colors=[1,1,1], autoLog=False):
        tStart = None
        tStop = None
        tStartRefresh = None
        tStopRefresh = None
        frameNStart = None
        frameNStop = None
        
        if elementMask == "circle":
            if elementFill:
                self.__Elements = [Polygon(win, 
                    pos=xys[i], radius=sizes/2, edges=32, units=units,
                    lineColor=tuple(colors), fillColor=tuple(colors), autoLog=autoLog) for i in range(0,nElements)]
            else:
                self.__Elements = [Polygon(win, 
                    pos=xys[i], radius=sizes/2, units=units,
                    lineColor=tuple(colors), autoLog=autoLog) for i in range(0,nElements)]

        elif elementMask == "line": 
            self.__Elements = [ShapeStim(win, 
                vertices=[xys[i]-rotateCoord([sizes[0]/2,0],oris[i]), xys[i]+rotateCoord([sizes[0]/2,0],oris[i])], lineWidth=sizes[1], units=units,
                lineColor=tuple(colors), autoLog=autoLog, closeShape=False) for i in range(0,nElements)]

    @property
    def status(self):
        return self.__Elements[0].status
    
    @status.setter
    def status(self,val):
        for e in self.__Elements:
            e.status = val

    def setAutoDraw(self,toDraw):
        [e.setAutoDraw(toDraw) for e in self.__Elements]

def dprime(hits, misses, fas, crs):
    """ returns a dict with d-prime measures given hits, misses, false alarms, and correct rejections"""
    # Floors an ceilings are replaced by half hits and half FA's
    half_hit = 0.5 / (hits + misses)
    half_fa = 0.5 / (fas + crs)
 
    # Calculate hit_rate and avoid d' infinity
    hit_rate = hits / (hits + misses)
    if hit_rate == 1: 
        hit_rate = 1 - half_hit
    if hit_rate == 0: 
        hit_rate = half_hit
 
    # Calculate false alarm rate and avoid d' infinity
    fa_rate = fas / (fas + crs)
    if fa_rate == 1: 
        fa_rate = 1 - half_fa
    if fa_rate == 0: 
        fa_rate = half_fa
 
    # Return d'
    return Z(hit_rate) - Z(fa_rate)