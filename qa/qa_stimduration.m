T = readtable('..\data\_repetitive Match-to-Sample_2022_Apr_08_2317.log','FileType','text');
T(1:8,:) = [];

indResp = find(strcmp(T.Var3,'Response - STOPPED'));
indStim = find(strcmp(T.Var2,'DATA'));

stimDuration = cellfun(@(str) str2double(regexp(str,'(?<=Duration: )[0-9\.]*','match')),T.Var3(indStim));
endStim = T.Var1(indStim) + stimDuration;

indLastResp = arrayfun(@(i) max(indResp(find(indResp>i,9,"first"))), indStim);
endLastResp = T.Var1(indLastResp);

histogram(endStim - endLastResp,-1:1e-1:1)