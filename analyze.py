from psychopy.tools.filetools import fromFile
import os, glob, csv
import numpy as np
from utils import dprime

dataDir = r'D:\Projects\bayesian'
nPart = 7

fPrec = open('precision.csv','w',newline='')
prec = csv.writer(fPrec, delimiter='\t')
#prec.writerow(['participant','session','rt','duration','precision'])
prec.writerow(['session','rt','duration','precision', 'd-prime'])
precDat = dict()

fRep = open('repetitive.csv','w',newline='')
rep = csv.writer(fRep, delimiter='\t')
# rep.writerow(['participant','session','rt','code'])
rep.writerow(['session','rt1','code1', 'd-prime1','rt2','code2', 'd-prime2','rt3','code3', 'd-prime3'])
repDat = dict()
respList = ['hit','miss','false','cr']

for pInd in range(1,nPart+1):
    p = 'p' + str(pInd)
    for fname in glob.glob(os.path.join(dataDir,p,'*precision*.psydat')):
        dat = fromFile(fname)
        val = np.mean(np.array([[trial['resp.rt'],trial['resp.duration'],trial['resp.precision'],trial['resp.dprime']] if trial['resp.precision'] != 'miss' else [3,3,0,dprime(0,dat.extraInfo['sampleNum'],0,dat.extraInfo['gridXY']**2-dat.extraInfo['sampleNum'])] for trial in dat.entries]),0)

        # prec.writerow([p,dat.entries[0]['session']] + val.tolist())

        if not(any([k == dat.extraInfo['session'] for k in precDat.keys()])):
            precDat[dat.extraInfo['session']] = np.zeros((nPart,4))
        precDat[dat.extraInfo['session']][pInd-1,:] = val

    for fname in glob.glob(os.path.join(dataDir,p,'*repetitive*.psydat')):
        dat = fromFile(fname)
        val = np.empty((0))
        for m in range(dat.extraInfo['nMatch']):
            val = np.append(val,np.mean(np.array([[trial['resp.rt'],trial['resp.code'] != 'false'] if trial['resp.code'] != 'miss' else [2,0] for trial in dat.entries[m::dat.extraInfo['nMatch']]]),0))

            # d-prime
            resps = [trial['resp.code'] for trial in dat.entries[m::dat.extraInfo['nMatch']]]
            respStat = np.zeros(len(respList))
            for i in range(len(respList)):
                respStat[i] = sum([r == respList[i] for r in resps])
            val = np.append(val,dprime(*respStat))

            # rep.writerow([p,dat.entries[0]['session']] + val.tolist())

        if not(any([k == dat.extraInfo['session'] for k in repDat.keys()])):
            repDat[dat.extraInfo['session']] = np.zeros((nPart,dat.extraInfo['nMatch']*3))
        repDat[dat.extraInfo['session']][pInd-1,:] = val

for s in precDat.keys():
    prec.writerow([s] + np.mean(precDat[s],0).tolist())

for s in repDat.keys():
    rep.writerow([s] + np.mean(repDat[s],0).tolist())

fPrec.close()

fRep.close()